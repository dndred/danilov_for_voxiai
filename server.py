from async_server import AsyncServer
from settings import host, port
from task_maker import TaskMaker, Task, Revers, Shift

__DEBUG__ = False


class TaskMakerAdapter(TaskMaker):
    available_commands = [Revers, Shift]

    def get_task_by_name(self, task_name):
        for task in self.available_commands:
            if task.task_name == task_name:
                return task
        return None


class CmdServer(AsyncServer):
    STATUS_COMMAND_NOT_FOUND = 'Command not found'
    STATUS_TASK_NOT_FOUND = 'Task not found'

    def __init__(self, host='localhost', port=8087):
        super().__init__(host, port)
        self.task_maker = TaskMakerAdapter()

    def stop(self):
        self.task_maker.ask_stop()
        super().stop()

    def run_server(self):
        self.task_maker.run_tasks()
        super().run_server()

    def process_request(self, request: bytes) -> str:
        request = request.decode('utf8')
        if '\n' in request:
            cmd, data = request.split('\n', maxsplit=1)
        else:
            cmd, data = request, ''
        code, result = self.dispatch_cmd(cmd, data)
        if result:
            return f'{code},{result}'
        else:
            return code

    def dispatch_cmd(self, cmd_name: str, data: str) -> (str, str):
        if __DEBUG__:
            print(cmd_name)

        if cmd_name.startswith('add '):
            return self.cmd_add(cmd_name, data)

        elif cmd_name.startswith('check '):
            task_id = cmd_name.split('check ')[1]
            return self.cmd_check(task_id)

        elif cmd_name.startswith('result '):
            task_id = cmd_name.split('result ')[1]
            return self.cmd_result(task_id)

        return self.STATUS_COMMAND_NOT_FOUND, ''

    def cmd_add(self, cmd_name, data):
        task_name = cmd_name.split('add ')[1]
        task = self.task_maker.get_task_by_name(task_name)
        if not task:
            return self.STATUS_COMMAND_NOT_FOUND, ''
        task_id = self.task_maker.add_task(task(data))
        return self.STATUS_OK, task_id

    def cmd_check(self, task_id):
        status = self.task_maker.check_task_status(task_id)
        if not status:
            return self.STATUS_TASK_NOT_FOUND, ''
        else:
            return self.STATUS_OK, status

    def cmd_result(self, task_id):
        status = self.task_maker.check_task_status(task_id)
        if status != Task.STATUS_DONE:
            return self.STATUS_TASK_NOT_FOUND, ''

        result = self.task_maker.get_task_result(task_id)
        return self.STATUS_OK, result


server = CmdServer(host, port)
server.run_server()
