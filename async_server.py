import asyncio
import signal
from sys import platform

__DEBUG__ = False


class AsyncServer:
    STATUS_OK = 'OK'
    STATUS_SERVER_ERROR = 'SERVER ERROR'

    def __init__(self, host='localhost', port=8087):
        self.host = host
        self.port = port

    def stop_server(self, *args):    
        asyncio.get_event_loop().stop()

    def run_server(self):        
        loop = asyncio.get_event_loop()
        
        signals = [signal.SIGTERM, signal.SIGINT]
        if platform == "linux" or platform == "linux2":
            signals.append(signal.SIGHUP)
            
        for s in signals:
            signal.signal(signal.SIGINT, self.stop_server)
            signal.signal(signal.SIGTERM, self.stop_server)            

        loop.create_task(asyncio.start_server(self.handle_client, self.host, self.port))
        loop.run_forever()

    async def handle_client(self, reader, writer):
        # assert isinstance(reader, StreamReader)

        request = bytearray()
        while True:
            chunk = await reader.read(1024)
            reader.feed_eof()
            request += chunk
            if reader.at_eof():
                break

        if __DEBUG__:
            response = self.process_request(request)
        else:
            try:
                response = self.process_request(request)
            except Exception as e:
                print(e)
                response = self.STATUS_SERVER_ERROR

        if __DEBUG__:
            print(response)
        writer.write(str.encode(response))
        writer.close()

    def process_request(self, request: bytes) -> str:
        pass
