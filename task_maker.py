import asyncio

__DEBUG__ = False


class Task:
    task_name = None
    STATUS_IN_QUEUE = 'In queue'
    STATUS_RUN = 'Run'
    STATUS_DONE = 'Done'

    def __init__(self, data: str):
        self.data = data
        self.result = ''
        self.task_id = None
        self.status = self.STATUS_IN_QUEUE

    def do(self):
        raise NotImplemented

    def __str__(self):
        return '{task_name} ({task_id}): {status} = {result}'.format(task_name=self.task_name,
                                                                     task_id=self.task_id,
                                                                     status=self.status,
                                                                     result=self.result)


class Revers(Task):
    task_name = 'revers'

    async def do(self):
        self.status = self.STATUS_RUN
        await asyncio.sleep(3)
        self.result = self.data[::-1]
        self.status = self.STATUS_DONE


class Shift(Task):
    task_name = 'shift'

    async def do(self):
        self.status = self.STATUS_RUN
        await asyncio.sleep(7)
        data = self.data
        self.result = ''.join([(data[i + 1] if len(data) > i + 1 else '') + data[i] for i in range(0, len(data), 2)])

        # result = []
        # for i in range(0, len(data), 2):
        #     prev = data[i + 1] if len(data) > i + 1 else ''
        #     result.append(prev + data[i])
        # self.result = ''.join(result)

        self.status = self.STATUS_DONE


class TaskMaker:
    def __init__(self):
        self.task_queue = []
        self.task_by_id = {}
        self._run_task = None
        self._current_task_num = 0
        self._do_stop = False

    def get_task_result(self, task_id):
        task = self.task_by_id.get(task_id, None)
        assert isinstance(task, Task)
        if not task or task.status != task.STATUS_DONE:
            return None
        return task.result

    def check_task_status(self, task_id):
        task = self.task_by_id.get(task_id, None)
        # assert isinstance(task, Task)
        if not task:
            return None
        return task.status

    def add_task(self, task: Task):
        self.task_queue.append(task)
        task.task_id = str(self.task_queue.index(task))
        self.task_by_id[task.task_id] = task
        return task.task_id

    def run_tasks(self, loop=None):
        if not loop:
            loop = asyncio.get_event_loop()
            self._run_task = loop.create_task(self.do_tasks_loop())

    def ask_stop(self):
        self._do_stop = True

    async def do_tasks_loop(self):
        while not self._do_stop:
            await self.do_task()
            await asyncio.sleep(0.001)

    async def do_task(self):
        if __DEBUG__:
            if self.task_queue:
                for task in self.task_queue:
                    print(task)
                print()

        if len(self.task_queue) > self._current_task_num:
            task = self.task_queue[self._current_task_num]
            await task.do()
            self._current_task_num += 1
