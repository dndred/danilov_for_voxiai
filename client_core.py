from socket import socket, AF_INET, SOCK_STREAM
from time import sleep

from settings import host, port


class TaskNotFound(Exception):
    pass


class CommandNotFound(Exception):
    pass


class Client:
    def send_and_read(self, data):
        tcp_socket = socket(AF_INET, SOCK_STREAM)
        tcp_socket.connect((host, port))

        data = str.encode(data)
        tcp_socket.send(data)
        response = bytearray()
        while True:
            chunk = tcp_socket.recv(1024)
            response += chunk
            if not chunk:
                break
        tcp_socket.close()
        return response.decode('utf-8')

    def check_error(self, response):
        if response == 'Command not found':
            raise CommandNotFound()
        elif response == 'Task not found':
            raise TaskNotFound()

    def parse_response(self, response):
        self.check_error(response)
        return response.lstrip('OK,')

    def add_task(self, task_name, data):
        response = self.send_and_read(f'add {task_name}\n{data}')
        return self.parse_response(response)

    def check_status(self, task_id):
        response = self.send_and_read(f'check {task_id}')
        return self.parse_response(response)

    def get_result(self, task_id):
        response = self.send_and_read(f'result {task_id}')
        return self.parse_response(response)

    def batch(self, task_id):
        status = ''
        while status != 'Done':
            status = self.check_status(task_id)
            print(f'Статус: {status}')
            sleep(1)
        result = self.get_result(task_id)
        print(f'Результат: {result}')
