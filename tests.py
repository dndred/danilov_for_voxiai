import asyncio
import unittest


from task_maker import TaskMaker, Revers, Shift


class TaskMakerTestCase(unittest.TestCase):
    def setUp(self):
        self.task_maker = TaskMaker()
        self.loop = asyncio.new_event_loop()
        asyncio.set_event_loop(None)

    def tearDown(self):
        self.task_maker.ask_stop()

    def test_add_task(self):
        task = Revers('строка')
        self.assertIsNotNone(self.task_maker.add_task(task))

    def test_check_task_status(self):
        task = Revers('строка')
        task_id = self.task_maker.add_task(task)
        self.assertEqual(self.task_maker.check_task_status(task_id), task.STATUS_IN_QUEUE)

    def test_check_reverse_result(self):
        async def func():
            task = Revers('строка')
            task_id = self.task_maker.add_task(task)
            await self.task_maker.do_task()
            self.assertEqual(self.task_maker.check_task_status(task_id), task.STATUS_DONE)
            self.assertEqual(self.task_maker.get_task_result(task_id), 'акортс')

        self.loop.run_until_complete(func())

    def test_check_shift_result(self):
        async def func():
            task = Shift('строка')
            task_id = self.task_maker.add_task(task)
            await self.task_maker.do_task()
            self.assertEqual(self.task_maker.check_task_status(task_id), task.STATUS_DONE)
            self.assertEqual(self.task_maker.get_task_result(task_id), 'тсорак')

        self.loop.run_until_complete(func())


if __name__ == '__main__':
    unittest.main()
