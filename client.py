import argparse

from client_core import Client, TaskNotFound, CommandNotFound


def get_parser():
    parser = argparse.ArgumentParser()
    parser.set_defaults(func='')
    parser.add_argument('-batch', action='store_true', help='Пакетный режим', default=False)

    subparsers = parser.add_subparsers(help='Список команд')

    cmd_parser = subparsers.add_parser('add', help='Добавить задачу')
    cmd_parser.add_argument('task', action='store', help='Название задачи')
    cmd_parser.add_argument('data', action='store', help='Данные на обработку')
    cmd_parser.set_defaults(func='add')

    status_parser = subparsers.add_parser('status', help='Статус задачи по ее идентификатору')
    status_parser.add_argument('id', action='store', help='Идентификатор задачи')
    status_parser.set_defaults(func='status')

    result_parser = subparsers.add_parser('result', help='Результат задачи по ее идентификатору')
    result_parser.add_argument('id', action='store', help='Идентификатор задачи')
    result_parser.set_defaults(func='result')
    return parser


def process_args(parser):
    args = parser.parse_args()
    if not args.func:
        parser.print_help()
        return

    client = Client()
    try:
        # print(args)
        if not args.func:
            parser.print_help()
        elif args.func == 'add':
            task_name = args.task
            data = args.data
            task_id = client.add_task(task_name, data)
            print(f'Идентификатор задачи: {task_id}')
            if args.batch:
                client.batch(task_id)
        elif args.func == 'status':
            task_id = client.check_status(args.id)
            print(f'Статус задачи: {task_id}')
        elif args.func == 'result':
            task_id = client.get_result(args.id)
            print(f'Результат задачи: {task_id}')
    except TaskNotFound:
        print('Задача не найдена')
    except CommandNotFound:
        print('Команда не найдена')


if __name__ == '__main__':
    process_args(get_parser())
